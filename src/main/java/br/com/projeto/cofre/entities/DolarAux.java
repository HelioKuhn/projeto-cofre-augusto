package br.com.projeto.cofre.entities;

public class DolarAux {
	
	private DolarAux USDBRL;
	
	private Double high;

	public DolarAux getUSDBRL() {
		return USDBRL;
	}

	public void setUSDBRL(DolarAux usdBRL) {
		USDBRL = usdBRL;
	}

	public Double getHigh() {
		return high;
	}

	public void setHigh(Double high) {
		this.high = high;
	}

	@Override
	public String toString() {
		return "DolarAux [USDBRL=" + USDBRL + ", high=" + high + "]";
	}
	
	

}
