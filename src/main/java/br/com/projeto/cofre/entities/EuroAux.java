package br.com.projeto.cofre.entities;

public class EuroAux {
	
	private EuroAux EURBRL;
	
	private Double high;

	public EuroAux getEURBRL() {
		return EURBRL;
	}

	public void setEURBRL(EuroAux eurDBRL) {
		EURBRL = eurDBRL;
	}

	public Double getHigh() {
		return high;
	}

	public void setHigh(Double high) {
		this.high = high;
	}

	@Override
	public String toString() {
		return "EuroAux [EURBRL=" + EURBRL + ", high=" + high + "]";
	}

	
	
	

}
