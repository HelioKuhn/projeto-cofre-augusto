package br.com.projeto.cofre.entities;

public abstract class Moeda {

	private Double valor;

	public abstract String info();
	
	public abstract void converter(Double valor);

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

}
