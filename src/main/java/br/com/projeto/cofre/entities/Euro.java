package br.com.projeto.cofre.entities;

import java.io.IOException;

import com.google.gson.JsonSyntaxException;

import br.com.projeto.cofre.api.MoedaAPI;

public class Euro extends Moeda {

	private Double valor;

	private Double valorConvetido;
	
	public Euro() throws JsonSyntaxException, IOException {
		this.valorConvetido = 0.0;
		this.valor = MoedaAPI.getValorEuro();
	}

	@Override
	public String info() {
		return toString();
	}

	@Override
	public void converter(Double valor) {
		valorConvetido += (valor * this.valor);
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Double getValorConvetido() {
		return valorConvetido;
	}

	public void setValorConvetido(Double valorConvetido) {
		this.valorConvetido = valorConvetido;
	}

	@Override
	public String toString() {
		return "Euro [valor=" + valor + ", valorConvetido=" + valorConvetido + "]";
	}
	

}
