package br.com.projeto.cofre.programa;

import br.com.projeto.cofre.entities.Dolar;
import br.com.projeto.cofre.entities.Euro;

public class Main {

	public static void main(String[] args) throws Exception {
		
		Dolar dolar = new Dolar();
		System.out.println(dolar);
		
		Euro euro = new Euro();
		System.out.println(euro);
	}

}
