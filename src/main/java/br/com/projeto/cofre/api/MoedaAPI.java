package br.com.projeto.cofre.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import br.com.projeto.cofre.entities.DolarAux;
import br.com.projeto.cofre.entities.EuroAux;

public class MoedaAPI {
	
	private static String urlDolar  = "https://economia.awesomeapi.com.br/json/last/USD-BRL";
	
	private static String urlEuro  = "https://economia.awesomeapi.com.br/json/last/EUR-BRL";
	
	private static StringBuilder valorRetornoDolar = null;
	
	private static StringBuilder valorRetornoEuro = null;
	
	public static String buscarCotacaoDolar() throws IOException {
		
		URL conn = new URL(urlDolar);
		URLConnection urlConnection = conn.openConnection();
		InputStream resposta = urlConnection.getInputStream();
		

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resposta, "UTF-8") );
		StringBuilder valorRetorno = new StringBuilder();
		String valor;
		while((valor = bufferedReader.readLine()) != null) {
			valorRetorno.append(valor);
		}
		MoedaAPI.valorRetornoDolar = valorRetorno;
		return MoedaAPI.valorRetornoDolar.toString();
	}
	
	public static Double getValorDolar() throws JsonSyntaxException, IOException {
		
		Gson gson = new Gson();
		
		DolarAux dolarAux = gson.fromJson(MoedaAPI.buscarCotacaoDolar(), DolarAux.class);
		
		return dolarAux.getUSDBRL().getHigh();
	}
	
	
public static String buscarCotacaoEuro() throws IOException {
		
		URL conn = new URL(urlEuro);
		URLConnection urlConnection = conn.openConnection();
		InputStream resposta = urlConnection.getInputStream();
		

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resposta, "UTF-8") );
		StringBuilder valorRetorno = new StringBuilder();
		String valor;
		while((valor = bufferedReader.readLine()) != null) {
			valorRetorno.append(valor);
		}
		MoedaAPI.valorRetornoEuro = valorRetorno;
		return MoedaAPI.valorRetornoEuro.toString();
	}
	
	public static Double getValorEuro() throws JsonSyntaxException, IOException {
		
		Gson gson = new Gson();
		
		EuroAux euroAux = gson.fromJson(MoedaAPI.buscarCotacaoEuro(), EuroAux.class);
		
		return euroAux.getEURBRL().getHigh();
	}

}
